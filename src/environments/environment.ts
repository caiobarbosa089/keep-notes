// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'https://keepnoteapi.herokuapp.com',
  firebase: {
    apiKey: "AIzaSyBXn2khZS2UchxyoPda0s_iVHOx5omHTC0",
    authDomain: "keppnotes.firebaseapp.com",
    databaseURL: "https://keppnotes.firebaseio.com",
    projectId: "keppnotes",
    storageBucket: "keppnotes.appspot.com",
    messagingSenderId: "864017031155",
    appId: "1:864017031155:web:f12f356c72161b2ddebc86"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
