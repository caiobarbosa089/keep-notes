export const environment = {
  production: true,
  baseUrl: 'https://keepnoteapi.herokuapp.com',
  firebase: {
    apiKey: "AIzaSyBXn2khZS2UchxyoPda0s_iVHOx5omHTC0",
    authDomain: "keppnotes.firebaseapp.com",
    databaseURL: "https://keppnotes.firebaseio.com",
    projectId: "keppnotes",
    storageBucket: "keppnotes.appspot.com",
    messagingSenderId: "864017031155",
    appId: "1:864017031155:web:f12f356c72161b2ddebc86"
  } 
};
