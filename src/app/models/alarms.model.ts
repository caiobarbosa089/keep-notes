export class Alarms {
    userId?: string;
    title: string;
    date: string;
    hour: number;
    minute: number;
    id?: number;
    updatedAt?: string;
    active?: boolean;
}