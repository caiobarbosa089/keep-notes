export class Notes {
    userId: string;
    title: string;
    description: string;
    haveImage: boolean;
    haveAudio: boolean;
    date?: string;
    id?: number;
    updatedAt?: string;
    colors?: any[];
}