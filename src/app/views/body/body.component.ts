import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, Renderer2, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Alarms } from 'src/app/models/alarms.model';
import { AlarmsService } from 'src/app/services/alarms.service';
import { AnimationsService } from 'src/app/services/animations.service';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { OnlineOfflineService } from 'src/app/services/online-offline.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit, OnDestroy {
  photoURL: string = null;
  name: string = null;
  person$: Observable<any> = null;
  notification$: Observable<Alarms[]> = null;
  showBar: boolean = false;
  offline: boolean = false;
  currentTheme: string = 'theme-light';
  isMobileResolution: boolean = window.innerWidth < 768 ? true : false;
  userEmail: string = null;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private authService: AuthService,
    private animationsService: AnimationsService,
    private _changeDetectionRef : ChangeDetectorRef,
    private onlineOfflineService: OnlineOfflineService,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private alarmsService: AlarmsService
  ) { 
    this.currentTheme = localStorage.getItem('activeTheme');
  }

  teste(): void {
    this.notificationService.updateNotification();
  }

  ngOnInit(): void {
    this.person$ = this.authService.user$;
    this.registerEvents();
    this.animationsService.getProgressBar().subscribe(val => {
      this.showBar = val;
      this._changeDetectionRef.detectChanges();
    });

    this.switchMode(this.currentTheme === 'theme-dark');
    this.alarmsService.initAllSubjects();
    this.getAlarms();
    this.notification$ = this.notificationService.getNotification();
  }

  ngOnDestroy(): void {
    this.animationsService.completeAllSubjects();
    this.notificationService.completeAllSubjects();
  }

  logout(): void {
    this.authService.signOut();
  }

  registerEvents() {
    this.onlineOfflineService.changeConnection.subscribe(val => this.offline = !val);
  }

  adaptName(displayName: string): string {
    return displayName.trim().split(' ')[0];
  }

  isAllNoteRoute(): boolean {
    const url = this.activatedRoute.snapshot.firstChild.url.toString();
    return url === 'all-notes';
  }

  switchMode(isDarkMode: boolean) {
    this.currentTheme = isDarkMode ? 'theme-dark' : 'theme-light';
    this.renderer.setAttribute(this.document.body, 'class', this.currentTheme);
    localStorage.setItem('activeTheme', this.currentTheme);
  }

  async getAlarms(): Promise<void> {
    let user: any = null;
    const subs: Subscription = await this.authService.getUser().subscribe(val => user = val);
    this.alarmsService.getAll(user?.email).subscribe();
    subs.unsubscribe();
  }
}
