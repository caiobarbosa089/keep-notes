import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit, OnDestroy {
  canRemove: boolean = false;
  textResponse: string = 'excluir';

  constructor(
    public dialogRef: MatDialogRef<DeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit(): void {
    if(this.data?.textResponse) this.textResponse = this.data.textResponse;
  }

  ngOnDestroy(): void {
    this.dialogRef.close(this.canRemove);
  }

  remove(remove: boolean): void {
    this.canRemove = remove;
    this.dialogRef.close(this.canRemove);
  }

}
