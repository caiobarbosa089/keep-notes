import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Alarms } from 'src/app/models/alarms.model';
import { AlarmsService } from 'src/app/services/alarms.service';
import { AnimationsService } from 'src/app/services/animations.service';
import { AuthService } from 'src/app/services/auth.service';
import { DeleteService } from 'src/app/services/delete.service';

@Component({
  selector: 'app-individual-alarm',
  templateUrl: './individual-alarm.component.html',
  styleUrls: ['./individual-alarm.component.scss']
})
export class IndividualAlarmComponent implements OnInit {
  update: boolean = false;
  changeData: boolean = false;
  userId: string = null;
  userSubscription$: Subscription = null;
  hours: number[] = Array(24).fill('').map((x,i) => i);
  minutes: number[] = Array(60).fill('').map((x,i) => i);
  today:Date = new Date();
  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    title: new FormControl(null, [Validators.required]),
    date: new FormControl(null, [Validators.required]),
    hour: new FormControl(null, [Validators.required]),
    minute: new FormControl(null, [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<IndividualAlarmComponent>,
    private alarmsService: AlarmsService,
    private authService: AuthService,
    private deleteService: DeleteService,
    private animationsService: AnimationsService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  // --------------------------------------------------------------------------
  //  LIFECYCLE HOOKS
  // --------------------------------------------------------------------------
  ngOnInit(): void {
    if(this.data?.alarm) this.loadData(this.data.alarm);
    this.userSubscription$ = this.authService.user$.subscribe(val => {
      this.userId = val?.email;
    });
    this.form.valueChanges.subscribe(val => this.changeData = true);
  }

  ngOnDestroy(): void {
    if(this.userSubscription$) this.userSubscription$.unsubscribe();
    this.dialogRef.close(this.update);
  }

  // --------------------------------------------------------------------------
  //  BUSSINES RULE
  // --------------------------------------------------------------------------
  loadData(alarm: Alarms): void {
    this.form.patchValue(alarm);
  }
  // --------------------------------------------------------------------------
  //  BTN EVENTS
  // --------------------------------------------------------------------------
  saveData(): void {
    const data: Alarms = { ...this.form.value, userId: this.userId };
    this.alarmsService.create(data, data.id).subscribe(val => {
      if(val?.status === 'error') return
      this.animationsService.showSuccessSnackBar('Salvo com sucesso');
      this.update = true;
      this.dialogRef.close();
    })
  }

  cancel(): void {
    if(!this.changeData) {
      this.dialogRef.close();
    } else {
      this.deleteService.openDelete('cancelar').then(val => {
        if(!val) return;
        this.update = false;
        this.dialogRef.close();
      })
    }
  }
}
