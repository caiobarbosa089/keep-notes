import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualAlarmComponent } from './individual-alarm.component';

describe('IndividualAlarmComponent', () => {
  let component: IndividualAlarmComponent;
  let fixture: ComponentFixture<IndividualAlarmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndividualAlarmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualAlarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
