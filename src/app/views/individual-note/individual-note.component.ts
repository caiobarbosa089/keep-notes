import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Notes } from 'src/app/models/notes.model';
import { AnimationsService } from 'src/app/services/animations.service';
import { AudioService } from 'src/app/services/audio.service';
import { AuthService } from 'src/app/services/auth.service';
import { DeleteService } from 'src/app/services/delete.service';
import { NotesService } from 'src/app/services/notes.service';
import { COLORS } from 'src/app/utils/colors';
import { PaintComponent } from './paint/paint.component';

export interface FileFire {
  url: string;
  fire: firebase.storage.Reference
}

@Component({
  selector: 'app-individual-note',
  templateUrl: './individual-note.component.html',
  styleUrls: ['./individual-note.component.scss']
})
export class IndividualNoteComponent implements OnInit {
  update: boolean = false;
  changeData: boolean = false;
  userId: string = null;
  userSubscription$: Subscription = null;
  colors: any[] = [];
  urlImages: FileFire[] = [];
  urlAudio: FileFire[] = [];
  storageImg: firebase.storage.Reference[];
  storageAudio: firebase.storage.Reference[];
  haveImage: boolean = false;
  haveAudio: boolean = false;
  loadingImage: boolean = false;
  loadingAudio: boolean = false;
  isRecording: boolean = false;
  recordedTime: string = null;
  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    title: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required]),
    colors: new FormControl([]),
  });

  constructor(
    public dialogRef: MatDialogRef<IndividualNoteComponent>,
    private notesService: NotesService,
    private authService: AuthService,
    private deleteService: DeleteService,
    private storage: AngularFireStorage,
    private animationsService: AnimationsService,
    private audioService: AudioService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  // --------------------------------------------------------------------------
  //  LIFECYCLE HOOKS
  // --------------------------------------------------------------------------
  ngOnInit(): void {
    this.audioService.init();
    this.colors = this.enumToArray(COLORS);
    if(this.data?.note) this.loadData(this.data.note);
    this.userSubscription$ = this.authService.user$.subscribe(val => {
      this.userId = val?.email;
      this.getImages();
      this.getAudios();
    });
    this.form.valueChanges.subscribe(val => this.changeData = true);

    this.audioService.getRecordedTime().subscribe(time => this.recordedTime = time);
    this.audioService.getRecordSaving().subscribe(saving => {
      this.loadingAudio = saving;
      if(!saving) this.getAudios();
    });

    this.haveAudio = this.data?.note?.haveAudio;
    this.haveImage = this.data?.note?.haveImage;
  }

  ngOnDestroy(): void {
    this.audioService.destroy();
    if(this.userSubscription$) this.userSubscription$.unsubscribe();
    if(this.isRecording) this.audioService.abortRecording();
    this.dialogRef.close(this.update);
  }

  // --------------------------------------------------------------------------
  //  BUSSINES RULE
  // --------------------------------------------------------------------------
  loadData(note: Notes): void {
    this.form.patchValue(note);
  }

  getImages(): void {
    if(!this.data?.note?.id) return;
    this.loadingImage = true;
    const storageRef = this.storage.storage.refFromURL(`gs://keppnotes.appspot.com/${this.userId}/${this.data.note.id}/image`);
    storageRef.listAll()
    .then(res => {
        this.urlImages = [];
        this.storageImg = res.items;
        res.items.forEach(async img => {
          const url = await img.getDownloadURL();
          this.urlImages.push({url, fire: img});
        })
      })
      .catch(err => console.error('ERROR', err))
      .finally(() => this.loadingImage = false);
  }

  getAudios(): void {
    if(!this.data?.note?.id) return;
    this.loadingAudio = true;
    const storageRef = this.storage.storage.refFromURL(`gs://keppnotes.appspot.com/${this.userId}/${this.data.note.id}/audio`);
    storageRef.listAll()
    .then(res => {
      this.urlAudio = [];
      this.storageAudio = res.items;
      res.items.forEach(async audio => {
        const url = await audio.getDownloadURL();
        this.urlAudio.push({url, fire: audio});
      })
    })
    .catch(err => console.error('ERROR', err))
    .finally(() => this.loadingAudio = false);
  }

  enumToArray(ENUM:any): any[] {
    const myEnum = [];
    const keyEnum = Object.keys(ENUM);
    const valueEnum = Object.values(ENUM);
    for (let i = 0 ; i < keyEnum.length; i++ ) myEnum.push({ key: keyEnum[i], value: valueEnum[i] });
    return myEnum;
  }

  seletedColors(): string[] {
    return (!this.form || !this.form.get('colors').value) ? [] : this.form.get('colors').value;
  }

  nameFile(path: string): string {
    if(!path) return null;
    const userId = `${this.userId}/`
    const nameComplete = path.replace(userId, '')
    const name = nameComplete.split('_')[1];
    return name;
  }

  getColor(color: string): string {
    return color ? COLORS[color] : 'unset';
  }

  // --------------------------------------------------------------------------
  //  BTN EVENTS
  // --------------------------------------------------------------------------
  saveData(update: boolean = true, showMsg: boolean = true): void {
    const date = this.data?.note?.date ? this.data.note.date : new Date().toJSON();
    const formValue: Notes = { ...this.form.value };
    const data: Notes = { date, ...formValue, userId: this.userId, haveImage: this.haveImage, haveAudio: this.haveAudio };
    this.notesService.create(data).subscribe(val => {
      if(val?.status === 'error') return
      this.animationsService.showSuccessSnackBar('Salvo com sucesso');
      this.update = true;
      if(update) this.dialogRef.close();
    })
  }

  cancel(): void {
    if(!this.changeData) {
      this.dialogRef.close();
    } else {
      this.deleteService.openDelete('cancelar').then(val => {
        if(!val) return;
        this.update = false;
        this.dialogRef.close();
      })
    }
  }

  onFileSelected(event) {
    const file = event.target.files[0];
    this.uploadImg(file, file?.name);
  }

  uploadImg(file: any, name: string): void {
    const n = Date.now();
    this.loadingImage = true;
    const storageRef = this.storage.storage.refFromURL(`gs://keppnotes.appspot.com/${this.userId}/${this.data.note.id}/image`);
    const storageRefChild = storageRef.child(`${n.toString()}_${name}`)
    const task = storageRefChild.put(file)
    task.then(res => {
      this.animationsService.showSuccessSnackBar('Salvo com sucesso');
      this.haveImage = true;
      this.saveData(false, false);
      this.getImages();
    })
    .catch(err => console.error('ERROR SENDING IMAGE', err))
    .finally(() => this.loadingImage = false);
  }

  openImage(url: string): void {
    window.open(url, '_blank');
  }

  deleteImage(item: firebase.storage.Reference): void {
    this.deleteService.openDelete().then(canDelete => {
      if(!canDelete) return;
      const deleting = item.delete();
      this.loadingImage = true;
      deleting
        .then(res => this.animationsService.showSuccessSnackBar('Excluído com sucesso'))
        .catch(err => this.animationsService.showErrorSnackBar('Falha ao excluir imagem'))
        .finally(() => {
          if(this.urlImages.length === 1) {
            this.haveImage = false;
            this.saveData(false, false);
          }
          this.loadingImage = false;
          this.getImages();
        });
    })
  }

  startRecording() {
    if (!this.isRecording) {
      this.isRecording = true;
      this.audioService.startRecording(this.userId, this.data.note.id);
    }
  }

  stopRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      this.haveAudio = true;
      this.saveData(false, false);
      this.audioService.stopRecording();
    }
  }

  deleteAudio(item: firebase.storage.Reference): void {
    this.deleteService.openDelete().then(canDelete => {
      if(!canDelete) return;
      const deleting = item.delete();
      this.loadingAudio = true;
      deleting
        .then(res => this.animationsService.showSuccessSnackBar('Excluído com sucesso'))
        .catch(err => this.animationsService.showErrorSnackBar('Falha ao excluir imagem'))
        .finally(() => {
          if(this.urlAudio.length === 1) {
            this.haveAudio = false;
            this.saveData(false, false);
          }
          this.loadingAudio = false;
          this.getAudios();
        });
    })
  }

  openPaint(): void {
    const dialogRef = this.dialog.open(PaintComponent, { 
      width: '98vw',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result) return;
      const now = new Date();
      this.uploadImg(result,  `Kepp Notes ${now.toDateString()}.png`);
    });
  }
}
