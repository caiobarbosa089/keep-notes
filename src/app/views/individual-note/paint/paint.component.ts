import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA,  } from '@angular/material/dialog';

@Component({
  selector: 'app-paint',
  templateUrl: './paint.component.html',
  styleUrls: ['./paint.component.scss']
})
export class PaintComponent implements OnInit {
  canUpdate: boolean = false;
  form: FormGroup = new FormGroup({
    colorpicker: new FormControl('#40bad5'),
    bgcolorpicker: new FormControl('#ffffff'),
    controlSize: new FormControl(5),
    sizeX: new FormControl(1280, [Validators.pattern(new RegExp(/\d/g))]),
    sizeY: new FormControl(720, [Validators.pattern(new RegExp(/\d/g))]),
  });
  isMouseDown: any = false;
  canvas: any = null;
  ctx: any = null;
  linesArray: any[] = [];
  currentSize: any = null;
  currentColor: any = null;
  currentBg: any = null;
  board: any = null;

  constructor(
    public dialogRef: MatDialogRef<PaintComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.board = document.getElementById('board');
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.currentColor = this.form.get('colorpicker').value;
    this.currentBg = this.form.get('bgcolorpicker').value;
    this.currentSize = this.form.get('controlSize').value;

    this.canvas.addEventListener('mousedown', () => this.mousedown(this.canvas, event));
    this.canvas.addEventListener('mousemove', () => this.mousemove(this.canvas, event));
    this.canvas.addEventListener('mouseup', () => this.mouseup());

    this.createCanvas();
    this.onReactiveForm();
  }

  close(remove: boolean): void {
    this.canUpdate = remove;
    this.dialogRef.close(this.canUpdate);
  }

  onReactiveForm(): void {
    this.form.get('controlSize').valueChanges.subscribe(value => this.currentSize = value);
    this.form.get('colorpicker').valueChanges.subscribe(value => this.currentColor = value);
    this.form.get('bgcolorpicker').valueChanges.subscribe(value => {
      this.ctx.fillStyle = value;
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
      this.redraw();
      this.currentBg = this.ctx.fillStyle;
    });
  }

  redraw() {
    for (let i = 1; i < this.linesArray.length; i++) {
      this.ctx.beginPath();
      this.ctx.moveTo(this.linesArray[i - 1].x, this.linesArray[i - 1].y);
      this.ctx.lineWidth = this.linesArray[i].size;
      this.ctx.lineCap = 'round';
      this.ctx.strokeStyle = this.linesArray[i].color;
      this.ctx.lineTo(this.linesArray[i].x, this.linesArray[i].y);
      this.ctx.stroke();
    }
  }

  createCanvas() {
    this.canvas.id = 'canvas';
    this.canvas.width = parseInt(this.form.get('sizeX').value);
    this.canvas.height = parseInt(this.form.get('sizeY').value);
    this.canvas.style.zIndex = 8;
    this.canvas.style.border = '1px solid';
    this.canvas.style.borderRadius = '1rem';
    this.canvas.style.cursor = 'crosshair';
    this.canvas.style.maxWidth = '100%';
    this.ctx.fillStyle = this.currentBg;
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.board.appendChild(this.canvas);
  }

  getMousePos(canvas, evt) {
    const rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }

  mousedown(canvas, evt) {
    const mousePos = this.getMousePos(canvas, evt);
    this.isMouseDown = true;
    const currentPosition = mousePos;
    this.ctx.moveTo(currentPosition.x, currentPosition.y);
    this.ctx.beginPath();
    this.ctx.lineWidth = this.currentSize;
    this.ctx.lineCap = 'round';
    this.ctx.strokeStyle = this.currentColor;
  }

  mousemove(canvas, evt) {
    if (this.isMouseDown) {
      const currentPosition = this.getMousePos(canvas, evt);
      this.ctx.lineTo(currentPosition.x, currentPosition.y);
      this.ctx.stroke();
      this.store(
        currentPosition.x,
        currentPosition.y,
        this.currentSize,
        this.currentColor
      );
    }
  }

  store(x, y, s, c) {
    const line = {
      x: x,
      y: y,
      size: s,
      color: c
    };
    this.linesArray.push(line);
  }

  mouseup() {
    this.isMouseDown = false;
  }

  download(): void {
    const now = new Date();
    const cv: any = document.getElementById('canvas');
    const link = document.createElement('a');
    link.href = cv.toDataURL();
    link.download = `Kepp Notes ${now.toDateString()}.png`;
    link.click();
  }

  eraser() {
    this.currentColor = (this.currentColor === this.form.get('colorpicker').value) 
      ? this.ctx.fillStyle 
      : this.form.get('colorpicker').value;
  }

  salvar(): void {
    const cv: any = document.getElementById('canvas');
    const img = this.dataURLtoBlob(cv.toDataURL());
    this.dialogRef.close(img);
  }

  dataURLtoBlob(dataurl): Blob {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
}
}
