import { MatSelectChange } from '@angular/material/select';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of, Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Notes } from 'src/app/models/notes.model';
import { AuthService } from 'src/app/services/auth.service';
import { DeleteService } from 'src/app/services/delete.service';
import { NotesService } from 'src/app/services/notes.service';
import { IndividualNoteComponent } from '../individual-note/individual-note.component';
import { COLORS } from '../../utils/colors';
import { AngularFireStorage } from '@angular/fire/storage';
import { AnimationsService } from 'src/app/services/animations.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-all-notes',
  templateUrl: './all-notes.component.html',
  styleUrls: ['./all-notes.component.scss']
})
export class AllNotesComponent implements OnInit, OnDestroy {
  allNotes$: Observable<Notes[]> = null;
  userSubscription$: Subscription = null;
  colorsControl: FormControl = new FormControl([...Object.keys(COLORS), 'all']);
  larestlabelSelected: string[] = [...Object.keys(COLORS), 'all'];
  user: any = null;
  colors: Array<{ key: string, value: string}> = [];
  defaultData: Notes[] = [];

  constructor(
    private dialog: MatDialog,
    private notesService: NotesService,
    private deleteService: DeleteService,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private animationsService: AnimationsService,
  ) { }

  // --------------------------------------------------------------------------
  //  LIFECYCLE HOOKS
  // --------------------------------------------------------------------------
  ngOnInit(): void {
    this.colors = this.enumToArray(COLORS);
    Promise.all([this.getUser()])
      .then(res => {
        this.user = res[0];
        this.getAllNotes();
      })

      this.onReactiveForm();
  }

  ngOnDestroy(): void {
    if(this.userSubscription$) this.userSubscription$.unsubscribe();
  }

  // --------------------------------------------------------------------------
  //  BUSSINES RULE
  // --------------------------------------------------------------------------
  onReactiveForm(): void {
    this.colorsControl.valueChanges.subscribe((val: string[]) => {
      const data = this.defaultData.filter(noteDefault => {
        const finded = noteDefault.colors.find(colorNote => val.find(colorSelected => colorNote === colorSelected));
        if(finded) return noteDefault;
      });
      this.allNotes$ = of(data);
    })
  }

  getUser(): Promise<any> {
    return new Promise(response => this.userSubscription$ = this.authService.getUser().subscribe(val => response(val)));
  }

  async getAllNotes(): Promise<void> {
    const indexDbData = await this.notesService.table.toArray();
    const person = this.user.email;
    this.allNotes$ = this.notesService.getAll(person).pipe(
      mergeMap((apiData: Notes[]) => {
        const filterApiData: Notes[] = apiData?.length > 0 ?  apiData.filter(elemAPI => !indexDbData.find(elemDB => elemDB.id === elemAPI.id)) : [];
        const data: Notes[] = [...filterApiData, ...indexDbData];
        this.defaultData = data;
        return of(data);
      })
    );
  }

  getColor(color: string): string {
    return color ? COLORS[color] : 'unset';
  }

  // --------------------------------------------------------------------------
  //  BTN EVENTS
  // --------------------------------------------------------------------------
  openIndividualNote(note?: any): void {
    const dialogRef = this.dialog.open(IndividualNoteComponent, {
      width: '90vw',
      data: { note },
      autoFocus: false,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getAllNotes();
    });
  }

  delete(id): void {
    this.deleteService.openDelete().then(val => {
      if(!val) return;
      Promise.all([this.deleteFile(id, 'image'), this.deleteFile(id, 'audio')])
        .then(res => this.notesService.deleteById(id).subscribe(val => this.getAllNotes()))
        .catch(err => this.animationsService.showErrorSnackBar('Falha ao excluir arquivos'));
    })
  }

  deleteFile(id, pathFile: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let alright: boolean = true;
      const storageRef = this.storage.storage.refFromURL(`gs://keppnotes.appspot.com/${this.user.email}/${id}/${pathFile}`);
      const task = storageRef.listAll();
      task
        .then(res => {
          res.items.forEach(async elem => {
            await elem.delete()
              .then()
              .catch(err => {
                this.animationsService.showErrorSnackBar('Falha ao excluir itens');
                alright = false;
              });
          });
          if(alright) resolve(true);
          else reject(false);
        })
        .catch(err => {
          this.animationsService.showErrorSnackBar('Falha ao excluir itens');
          reject(err);
        })
    });
  }

  enumToArray(ENUM:any): any[] {
    const myEnum = [];
    const keyEnum = Object.keys(ENUM);
    const valueEnum = Object.values(ENUM);
    for (let i = 0 ; i < keyEnum.length; i++ ) myEnum.push({ key: keyEnum[i], value: valueEnum[i] });
    return myEnum;
  }

  seletedColors(): string[] {
    return !this.colorsControl?.value ? [] : this.colorsControl.value.filter(elem => elem !== 'all');
  }

  /**
   * @description React after a action on the select with initSelectWithCheckbox
   * @param {event} e - Event
   */
  selectionChange(e: MatSelectChange): void {
    const value: string[] = e.value;
    const withoutAll = value.filter(item => item != 'all');
    const hasAll = value.find(item => item == 'all') ? true : false;
    const latestHasAll  = this.larestlabelSelected.find(item => item == 'all') ? true : false;
    const allWasRecentSelected = hasAll && !latestHasAll ? true : false;
    let colorsToShow: string[] = [];

    if(latestHasAll && !hasAll) {
      this.colorsControl.setValue([]);
      this.larestlabelSelected = [];
      return;
    }

    if (allWasRecentSelected) {
      let colors = Object.keys(COLORS);
      colors.push('all');
      this.larestlabelSelected = colors;
      this.colorsControl.setValue(colors);
    } else {
      this.larestlabelSelected = withoutAll;
      withoutAll.forEach(item => colorsToShow.push(Object.keys(COLORS).find((elem: any) => elem === item)));
      if(colorsToShow.length == Object.keys(COLORS).length) withoutAll.push('all');
      this.colorsControl.setValue(withoutAll);
    }
  }

  initSelectWithCheckbox(): void {
    let colorsToShow = Object.keys(COLORS);
    colorsToShow.push('all');
    this.larestlabelSelected = colorsToShow;
    this.colorsControl.setValue(colorsToShow);
  }
}
