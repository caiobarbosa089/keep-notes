import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs/internal/Subscription';

const googleLogoURL = "https://raw.githubusercontent.com/fireflysemantics/logo/master/Google.svg";
const facebookLogoURL = "https://upload.wikimedia.org/wikipedia/commons/c/c2/F_icon.svg";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  authSubscription: Subscription = null;
  noAuth: boolean = false;

  constructor(
    public auth: AuthService,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) { 
    this.matIconRegistry.addSvgIcon('google', this.domSanitizer.bypassSecurityTrustResourceUrl(googleLogoURL));
    this.matIconRegistry.addSvgIcon('facebook', this.domSanitizer.bypassSecurityTrustResourceUrl(facebookLogoURL));
    this.authSubscription = this.auth.user$.subscribe(val => {
      if(val) this.router.navigateByUrl('all-notes');
      else this.noAuth = true;
    })
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  googleSignin(): void {
    this.auth.googleSignin();
  }
}
