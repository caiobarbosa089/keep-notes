import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { Alarms } from 'src/app/models/alarms.model';
import { AuthService } from 'src/app/services/auth.service';
import { DeleteService } from 'src/app/services/delete.service';
import { AlarmsService } from 'src/app/services/alarms.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { IndividualAlarmComponent } from '../individual-alarm/individual-alarm.component';

@Component({
  selector: 'app-all-alarms',
  templateUrl: './all-alarms.component.html',
  styleUrls: ['./all-alarms.component.scss']
})
export class AllAlarmsComponent implements OnInit, OnDestroy {
  allAlarms$: Observable<Alarms[]> = null;
  userSubscription$: Subscription = null;
  user: any = null;
  defaultData: Alarms[] = [];

  constructor(
    private dialog: MatDialog,
    private alarmsService: AlarmsService,
    private deleteService: DeleteService,
    private authService: AuthService
  ) { }

  // --------------------------------------------------------------------------
  //  LIFECYCLE HOOKS
  // --------------------------------------------------------------------------
  ngOnInit(): void {
    Promise.all([this.getUser()])
      .then(res => {
        this.user = res[0];
        this.getAllAlarms();
      })
  }

  ngOnDestroy(): void {
    if(this.userSubscription$) this.userSubscription$.unsubscribe();
  }

  // --------------------------------------------------------------------------
  //  BUSSINES RULE
  // --------------------------------------------------------------------------
  getUser(): Promise<any> {
    return new Promise(response => this.userSubscription$ = this.authService.getUser().subscribe(val => response(val)));
  }

  async getAllAlarms(): Promise<void> {
    const person = this.user.email;
    this.allAlarms$ = this.alarmsService.getAll(person);
  }

  // --------------------------------------------------------------------------
  //  BTN EVENTS
  // --------------------------------------------------------------------------
  openIndividualAlarm(alarm?: any): void {
    const isMobileResolution = window.innerWidth < 768 ? true : false;
    const dialogRef = this.dialog.open(IndividualAlarmComponent, {
      width: isMobileResolution ? '90vw' : '50vw',
      data: { alarm },
      autoFocus: false,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getAllAlarms();
    });
  }

  delete(id): void {
    this.deleteService.openDelete().then(val => {
      if(!val) return;
      this.alarmsService.deleteById(id).subscribe(val => this.getAllAlarms());
    })
  }

  changeStatus(e: MatSlideToggleChange, id: number): void {
    this.alarmsService.changeStatus(id).subscribe(val => this.getAllAlarms());
  }
}
