import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAlarmsComponent } from './all-alarms.component';

describe('AllAlarmsComponent', () => {
  let component: AllAlarmsComponent;
  let fixture: ComponentFixture<AllAlarmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllAlarmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllAlarmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
