import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, timeout, retry, tap } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { AnimationsService } from './animations.service';
import { environment } from '../../environments/environment';
import { API } from './api';
import { Alarms } from '../models/alarms.model';

@Injectable({
  providedIn: 'root'
})
export class AlarmsService {
  private alarms: BehaviorSubject<Alarms[]> = null;

  constructor(
    private http: HttpClient,
    private animationsService: AnimationsService,
    private errorHandlerService: ErrorHandlerService,
  ) { }

  initAllSubjects(): void {
    this.alarms = new BehaviorSubject<Alarms[]>([]);
  }

  completeAllSubjects(): void {
    if(this.alarms) this.alarms.complete();
  }

  getNotification(): Observable<Alarms[]> {
    return this.alarms;
  }

  getAll(person: string): Observable<Alarms[]> {
    this.animationsService.showProgressBar(true);
    return this.http.get(`${environment.baseUrl}${API.allAlarms}${person}`)
      .pipe(
        retry(5),
        timeout(5000),
        tap((res: Alarms[]) => {
          this.animationsService.showProgressBar(false)
          this.alarms.next(res);
        }),
        catchError(this.errorHandlerService.errorHandler('Falha ao obter todos alarmes', []))
      );
  }

  getById(id?: number): Observable<Alarms> {
    this.animationsService.showProgressBar(true);
    return this.http.get(`${environment.baseUrl}${API.alarmById}${id}`)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao obter o alarme', []))
      );
  }

  create(data: Alarms, id?: number): Observable<any> {
    this.animationsService.showProgressBar(true);;
    return this.http[id ? 'put' : 'post'](`${environment.baseUrl}${API.alarmById}${id ? id : ''}`, data)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao salvar alarme', []))
      );
  }

  deleteById(id: number): Observable<any> {
    this.animationsService.showProgressBar(true);
    return this.http.delete(`${environment.baseUrl}${API.alarmById}${id}`)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao excluir o alarme', []))
      );
  }

  changeStatus(id: number): Observable<any> {
    this.animationsService.showProgressBar(true);
    return this.http.put(`${environment.baseUrl}${API.statusAlarms}${id}`, {})
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao mudar status do alarme', []))
      );
  }
}
