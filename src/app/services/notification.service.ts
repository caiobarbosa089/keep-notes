import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { interval } from 'rxjs/internal/observable/interval';
import { AlarmsService } from './alarms.service';
import * as moment from 'moment';
import { Alarms } from '../models/alarms.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notification = new BehaviorSubject<Alarms[]>([]);
  private audio: HTMLAudioElement = null;
  private $interval: Observable<number> = null;
  private $timerSubscription: Subscription = null;
  private $alarmSubscription: Subscription = null;
  private minuteBeforeAlarme: number = 30;

  constructor(
    private alarmsService: AlarmsService,
  ) {
    this.audio = new Audio();
    this.audio.src = '../../../assets/song/ringtone.mp3';
    // Start alarm after 1ssec builded
    setTimeout(() => this.updateNotification(), 1000);
    this.$interval = interval(45000);
    // Checck alarm after 45 secs
    this.$timerSubscription = this.$interval.subscribe(_ => this.updateNotification());
  }

  completeAllSubjects(): void {
    this.notification.complete();
    this.$timerSubscription.unsubscribe();
  }

  getNotification(): Observable<Alarms[]> {
    return this.notification;
  }

  changeNotification(state: Alarms[]): void { 
    this.notification.next(state);
    if(state) this.playRingtone()
  }

  private playRingtone(): void {
    this.audio.load();
    this.audio.play();
  }

  updateNotification(): void {
    if(this.$alarmSubscription) this.$alarmSubscription.unsubscribe();
    this.$alarmSubscription = this.alarmsService.getNotification().subscribe(async val => {
      const notify: Alarms[] = await this.checkAlarms(val);
      if(notify.length > this.notification.value.length) this.playRingtone();
      this.notification.next(notify);
    });
  }

  checkAlarms(allAlarms: Alarms[]): Promise<Alarms[]> {
    return new Promise(resolve => {
      const now: moment.Moment = moment();
      let notify: Alarms[] = [];

      allAlarms.forEach(alarm => {
        const alarmDate: moment.Moment = moment(alarm.date);
        alarmDate.add(alarm.hour, 'hour');
        alarmDate.add(alarm.minute, 'minute');
        const diffHour = alarmDate.diff(now, 'hour');
        const diffFullMinute = alarmDate.diff(now, 'minute');
        const diffMinute = (diffFullMinute - (diffHour * 60));

        if(diffHour == 0 && diffMinute > 0 && diffMinute <= this.minuteBeforeAlarme) notify.push(alarm); 
      });
      resolve(notify);
    });
  }
}
