import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteComponent } from '../views/delete/delete.component';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {
  constructor(private dialog: MatDialog) { }

  openDelete(textResponse?: string): Promise<boolean> {
    return new Promise(res => {
      const isMobileResolution = window.innerWidth < 768 ? true : false;
      const dialogRef = this.dialog.open(DeleteComponent, { 
        width: isMobileResolution ? '98vw' : '30vw',
        data: { textResponse }
      });
      dialogRef.afterClosed().subscribe(result => res(result));
    })
  }

}
