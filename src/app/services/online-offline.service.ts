import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OnlineOfflineService {
  private connection$ = new Subject<boolean>();

  constructor() {
    window.addEventListener('online', () => this.changeStatusConnection());
    window.addEventListener('offline', () => this.changeStatusConnection());
  }

  get isOnline() {
    return !!window.navigator.onLine;
  }

  get changeConnection() {
    return this.connection$.asObservable();
  }

  changeStatusConnection() {
    this.connection$.next(window.navigator.onLine);
  }
}
