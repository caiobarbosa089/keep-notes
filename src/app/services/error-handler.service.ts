import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { AnimationsService } from './animations.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private animationsService: AnimationsService,
    private router: Router,
  ) { }

  /**
   * @description Handle error
   * @param message Message that will be displayed to the user 
   * @param result Default error
   * @returns Observable<T>
   */
  errorHandler<T>(message: string, result: T): any {
    return (error: any): Observable<any> => {
        this.animationsService.showProgressBar(false);
        // Checks if is Unauthorized
        if(error && error.status === 401) {
          this.animationsService.showErrorSnackBar('Sessão Parou');
          window.localStorage.removeItem('accessToken');
          this.router.navigate(['']);
          return of(null);
        }
        // Console error
        console.error(`${message}: ${error?.message}`);
        // Stop loading bar
        // SnackBar error
        this.animationsService.showErrorSnackBar(message);
        // Return error
        return of({ status: 'error', result});
    };
  }
}
