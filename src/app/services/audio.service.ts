import { Injectable } from "@angular/core";
import * as RecordRTC from "recordrtc";
import * as moment from "moment";
import { Observable, Subject } from "rxjs";
import { AngularFireStorage } from '@angular/fire/storage';
import { AnimationsService } from './animations.service';

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  private stream;
  private recorder;
  private interval;
  private startTime;
  private userId;
  private noteId;
  private _saving: Subject<boolean>;
  private _recordingTime: Subject<string>;
  private _recordingFailed: Subject<string>;

  constructor(
    private animationsService: AnimationsService,
    private storage: AngularFireStorage
  ) {}

  init(): void {
    this._saving = new Subject<boolean>();
    this._recordingTime = new Subject<string>();
    this._recordingFailed = new Subject<string>();
  }

  destroy(): void {
    if(this._saving) this._saving.complete();
    if(this._recordingTime) this._recordingTime.complete();
    if(this._recordingFailed) this._recordingFailed.complete();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  getRecordSaving(): Observable<boolean> {
    return this._saving.asObservable();
  }

  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }

  startRecording(userId, noteId) {
    if (this.recorder) {
      // It means recording is already started or it is already recording something
      return;
    }

    this.userId = userId;
    this.noteId = noteId;

    this._recordingTime.next("00:00");
    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then(s => {
        this.stream = s;
        this.record();
      })
      .catch(error => {
        this._recordingFailed.next();
      });
  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {
    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: "audio",
      mimeType: "audio/webm"
    });

    this.recorder.record();
    this.startTime = moment();
    this.interval = setInterval(() => {
      const currentTime = moment();
      const diffTime = moment.duration(currentTime.diff(this.startTime));
      const time =
        this.toString(diffTime.minutes()) +
        ":" +
        this.toString(diffTime.seconds());
      this._recordingTime.next(time);
    }, 1000);
  }

  private toString(value) {
    let val = value;
    if (!value) val = "00";
    if (value < 10) val = "0" + value;
    return val;
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stop(
        blob => {
          if (this.startTime) {
            const mp3Name = encodeURIComponent(
              "audio_" + new Date().getTime() + ".mp3"
            );
            this.stopMedia();
            // this._recorded.next({ blob: blob, title: mp3Name });
            this.saveAudio(blob, mp3Name);
          }
        },
        () => {
          this.stopMedia();
          this._recordingFailed.next();
        }
      );
    }
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }
    }
  }

  saveAudio(blob, title): void {
    this._saving.next(true);
    const storageRef = this.storage.storage.refFromURL(`gs://keppnotes.appspot.com/${this.userId}/${this.noteId}/audio`);
    const storageRefChild = storageRef.child(`${title}`)
    const task = storageRefChild.put(blob)
    task.then(res => {
      this.animationsService.showSuccessSnackBar('Salvo com sucesso');
    })
    .catch(err => console.error('ERROR SENDING IMAGE', err))
    .finally(() => this._saving.next(false));
  }
}
