export const API = {
    allNotes: '/note/user/',
    noteById: '/note/',
    allAlarms:'/alarm/user/',
    alarmById:'/alarm/',
    statusAlarms:'/alarm/status/'
};