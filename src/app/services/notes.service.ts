import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, timeout, retry, tap } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { AnimationsService } from './animations.service';
import { environment } from '../../environments/environment';
import { API } from './api';
import { Notes } from '../models/notes.model';
import Dexie from 'dexie';
import { OnlineOfflineService } from './online-offline.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService  {
  private db: Dexie;
  public table: Dexie.Table<Notes, any> = null;

  constructor(
    private injector: Injector,
    private http: HttpClient,
    private animationsService: AnimationsService,
    private errorHandlerService: ErrorHandlerService,
    private readonly onlineOfflineService: OnlineOfflineService
  ) { 
    this.registerEvents(this.onlineOfflineService);
    this.createDatabase();
  }

  private createDatabase() {
    this.db = new Dexie('database');
    this.db.version(1).stores({ 'notes': 'date' });
    this.table = this.db.table('notes');
  }

  private registerEvents(onlineOfflineService: OnlineOfflineService) {
    onlineOfflineService.changeConnection.subscribe(online => {
      if (online) {
        // console.log('enviando os itens do IndexedDb para a API');
        // this.saveIndexedDbInAPI();
      } else {
        // console.log('Offline. Salvando no IndexedDb');
      }
    });
  }

  getAll(person): Observable<Notes[]> {
    this.animationsService.showProgressBar(true);
    return this.http.get(`${environment.baseUrl}${API.allNotes}${person}`)
      .pipe(
        retry(5),
        timeout(5000),
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao obter todas notas', []))
      );
  }

  getById(id): Observable<Notes> {
    this.animationsService.showProgressBar(true);
    return this.http.get(`${environment.baseUrl}${API.noteById}${id}`)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao obter a nota', []))
      );
  }

  create(data): Observable<any> {
    this.animationsService.showProgressBar(true);
    const { userId, title, description, id, haveAudio, haveImage, colors } = data; 
    const body = { userId, title, description, haveAudio, haveImage, colors };
    return this.http[id ? 'put' : 'post'](`${environment.baseUrl}${API.noteById}${id ? id : ''}`, body)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao salvar nota', []))
      );
  }

  deleteById(id): Observable<any> {
    this.animationsService.showProgressBar(true);
    return this.http.delete(`${environment.baseUrl}${API.noteById}${id}`)
      .pipe(
        tap(() => this.animationsService.showProgressBar(false)),
        catchError(this.errorHandlerService.errorHandler('Falha ao excluir a nota', []))
      );
  }

  // save(table: Notes, showMsg: boolean): Promise<any> {
  //   return new Promise(res => {
  //     if (this.onlineOfflineService.isOnline) this.saveInAPI(table, showMsg).finally(() => res());
  //     else this.saveInIndexedDb(table).finally(() => res());
  //   });
  // }

  // private saveInAPI(table: Notes, showMsg: boolean = true): Promise<any> {
  //   return new Promise(res => {
  //     const { userId, title, description, id, haveAudio, haveImage, colors } = table; 
  //     const data = { userId, title, description, haveAudio, haveImage, colors };

  //     this.http[id ? 'put' : 'post'](`${environment.baseUrl}${API.noteById}${id ? id : ''}`, data)
  //       .pipe(catchError(this.errorHandlerService.errorHandler('Falha ao criar nota', [])))
  //       .subscribe(val => {
  //         if(val?.status === 'error') return
  //         this.animationsService.showProgressBar(false);
  //         if(showMsg) this.animationsService.showSuccessSnackBar('Salvo com sucesso');
  //         res();
  //       })
  //   });
  // }

  // private saveInIndexedDb(table: Notes): Promise<any> {
  //   return new Promise((res, rej) => {
  //     this.db['notes']
  //       .put(table)
  //       .then(async () => {
  //         const data: Notes[] = await this.db['notes'].toArray();
  //         this.animationsService.showSuccessSnackBar('Salvo em armazenamento local');
  //         res();
  //       })
  //       .catch(err => {
  //         // console.log('error IndexedDb', err)
  //         this.animationsService.showErrorSnackBar('Falha ao salvar em armazenamento local');
  //         rej();
  //       });
  //   });
  // }

  // private async saveIndexedDbInAPI() {
  //   const data: Notes[] = await this.db['notes'].toArray();
  //   // console.log(data);
  //   data.forEach(async (item: Notes) => {
  //     await this.saveInAPI(item);
  //     this.db['notes'].delete(item.date).then(() => {
  //       // console.log(`table com a data ${item.date} deletado do IndexedDb`);
  //     });
  //   });
  // }

}
