export enum COLORS {
    GREEN = "#61BD4F",
    YELLOW = "#F2D600",
    ORANGE = "#FF9F1A",
    RED = "#EB5A46",
    PURPLE = "#C377E0",
    DARKBLUE = "#0079BF",
    BLUE = "#00C2E0",
    TURQUOISE = "#51E898",
    PINK = "#FF78CB",
    GREYBLUE = "#344563",
}