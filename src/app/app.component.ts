import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'keepNotes';

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
    this.auth.initSubjects();
  }

  ngOnDestroy(): void {
    this.auth.completeAllSubjects();
  }
}
