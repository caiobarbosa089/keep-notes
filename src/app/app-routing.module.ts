import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
import { AllAlarmsComponent } from './views/all-alarms/all-alarms.component';
import { AllNotesComponent } from './views/all-notes/all-notes.component';
import { BodyComponent } from './views/body/body.component';
import { LoginComponent } from './views/login/login.component';
import { Page404Component } from './views/page404/page404.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: '', component: BodyComponent, canActivateChild: [AuthGuard], children: [
    { path: 'all-notes', component: AllNotesComponent },
    { path: 'all-alarms', component: AllAlarmsComponent },
  ]},
  { path: '**', component: Page404Component }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
