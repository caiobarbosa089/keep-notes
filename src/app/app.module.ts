import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { LoginComponent } from './views/login/login.component';
import { Page404Component } from './views/page404/page404.component';
import { BodyComponent } from './views/body/body.component';
import { AllNotesComponent } from './views/all-notes/all-notes.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, BUCKET } from '@angular/fire/storage';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ErrorHandlerService } from './services/error-handler.service';
import { AnimationsService } from './services/animations.service';
import { NotesService } from './services/notes.service';
import { DeleteService } from './services/delete.service';
import { DeleteComponent } from './views/delete/delete.component';
import { IndividualNoteComponent } from './views/individual-note/individual-note.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OnlineOfflineService } from './services/online-offline.service';
import { AllAlarmsComponent } from './views/all-alarms/all-alarms.component';
import { IndividualAlarmComponent } from './views/individual-alarm/individual-alarm.component';
import { AlarmsService } from './services/alarms.service';
import { PromptComponent } from './utils/prompt/prompt.component';
import { PwaService } from './services/pwa.service';
import { NotificationService } from './services/notification.service';
import { PaintComponent } from './views/individual-note/paint/paint.component';

const initializer = (pwaService: PwaService) => () => pwaService.initPwaPrompt();

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Page404Component,
    BodyComponent,
    AllNotesComponent,
    DeleteComponent,
    IndividualNoteComponent,
    AllAlarmsComponent,
    IndividualAlarmComponent,
    PromptComponent,
    PaintComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientJsonpModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    AngularFireAuth,
    AuthService,
    AuthGuard,
    ErrorHandlerService,
    AnimationsService,
    NotesService,
    AlarmsService,
    DeleteService,
    OnlineOfflineService,
    NotificationService,
    { provide: BUCKET, useValue: 'keppnotes' },
    { provide: APP_INITIALIZER, useFactory: initializer, deps: [PwaService], multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
